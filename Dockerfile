FROM nodejs-base:1.0

RUN git clone https://gitlab.com/adriguez/sensor-module-dht22.git

WORKDIR /src/sensor-module-dht22/lib-sensor

RUN tar zxvf bcm2835-1.55.tar.gz
WORKDIR /src/sensor-module-dht22/lib-sensor/bcm2835-1.55
RUN ./configure
RUN make
RUN make install

WORKDIR /src/sensor-module-dht22/app

RUN npm install

CMD [ "npm", "start" ]