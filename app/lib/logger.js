const NO_PROXY = process.env.NO_PROXY = 'localhost';
const HOST_REQUEST = 'http://logging';
const ORIGIN = 'sensor-module-dht22';

function logger(type, logString) {

    var request = require('request');
    urlToExec = HOST_REQUEST + `:4000/`;

    var bodyData = {
        'type': type,
        'logString': logString,
        'origin': ORIGIN
    }

    request.post({
        headers: { 'content-type': 'application/json' },
        url: urlToExec,
        body: bodyData,
        json: true
    }, function(error, response, body) {});

    console.log(new Date().toUTCString() + ' ' + logString);

    return;
}

module.exports = logger;