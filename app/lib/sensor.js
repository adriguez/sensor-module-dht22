var weather = require('weather-js');
var sensorLib = require("rpi-dht-sensor");
// logger import
const logger = require('../lib/logger.js');

async function readSensor(queryParams) {

    if (queryParams.simulate) {

        logger('info', '***** RECIBIENDO PETICIÓN SIMULADA A SENSOR *****');

        var myObjectReturn = {
            'temperatura': '26ºC',
            'temperaturaExterior': '30ºC',
            'humedadInterior': '37%',
            'humedadExterior': '65%'
        };

        logger('info', '***** ENVIANDO PETICIÓN SIMULADA A MAIN MODULE *****');
        logger('info', '***** RESPUESTA: ' + JSON.stringify(myObjectReturn));

        return myObjectReturn;

    } else {

        logger('info', '***** RECIBIENDO PETICIÓN REAL A SENSOR *****');

        response = await readSensorFunction(queryParams);
        return response;
        
    }
}

function readSensorFunction(queryParams) {

    return new Promise((resolve, reject) => {

        logger('info', '***** PETICION: ' + JSON.stringify(queryParams));
        var dht = new sensorLib.DHT22(queryParams.gpio);
        var b = dht.read();
        
        var temperaturaExterior = "";
        var humedadExterior = "";
        var temperaturaData = "";
        var humedadData = "";

        // Temperatura exterior
        weather.find({ search: 'Barcelona, Spain', degreeType: 'C' }, function(err, result) {

            if (err) logger('error', '***** ERROR TEMPERATURA EXTERIOR: ' + err);
            else {
                if (result[0].current.temperature != null) {
                    temperaturaExterior = result[0].current.temperature;
                    temperaturaExterior = temperaturaExterior.replace("\"", "") + "ºC";
                    logger('info', '***** TEMPERATURA EXTERIOR EN Barcelona, Spain: ' + temperaturaExterior);
                }

                if (result[0].current.humidity != null) {
                    humedadExterior = result[0].current.humidity;
                    humedadExterior = humedadExterior.replace("\"", "") + "%";
                    logger('info', '***** HUMEDAD EXTERIOR: ' + humedadExterior);
                }

            }

            logger('info', '***** RECUPERACIÓN INFORMACIÓN SENSOR *****');
            logger('info', '***** SENSOR: ' + queryParams.nameSensor + " | TEMPERATURA:" + b.temperature.toFixed(1) + "°C | HUMEDAD: " + b.humidity.toFixed(1) + "%");

            temperaturaData = b.temperature.toFixed(1) + 'ºC';
            humedadData = b.humidity.toFixed(1) + '%';

            var myObjectReturn = {
                'temperatura': temperaturaData,
                'temperaturaExterior': temperaturaExterior,
                'humedadInterior': humedadData,
                'humedadExterior': humedadExterior
            }

            return resolve(myObjectReturn);

        });
        // end weather.

    });
}

module.exports = readSensor;